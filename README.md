##repositorio Andres duque chavarriaga

## Fechas de Evaluación

1. **Entregable 1:** 7/Septiembre/2023
2. **Entregable 2:** 28/Septiembre/2023
3. **Parcial 1:** 5/Octubre/2023
4. **Entregable 3:** 2/Noviembre/2023
5. **Entregable 4:** 30/Noviembre/2023
6. **Parcial 2:** 7/Diciembre/2023

## Herramientas Utilizadas

- **Git:** [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code:** [Sitio oficial](https://code.visualstudio.com/)
- **Python 3.11.4:** [Descargar](https://www.python.org/downloads/release/python-3114/)
- **Extenciones:** Git Grafh, Git Lens, Jupyter, Material Icon, Excel Viewer

## Control de Versiones

- **git clone** `<url>`
- **git add .**  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

- **python -m venv venv** Creacion de espacio de trabajo
- **.\venv\Scripts\activate** Activar entorno virtual

## Librerias

- **python.exe -m pip install --upgrade pip** Actualizaciones
- **pip install** `<lib>` (pandas, openpyxl, lxml, jupyter)
- **pip list** Mostrar instalaciones
- **jupyter notebook** Interfaz web para trabajar con cuadernos

## Sesiones

| Sesión  | Fecha      | Tema                                                                   |
| ------- | ---------- | -----------------------------------------------------------------------|
| 1       | 10/08/2023 | Introducción a la programación, Control de versiones                   |
| 2       | 16/08/2023 | Tipos de variables, Estructura de datos "\Sesiones\Sesion2\Sesion2.py" |
| 3       | 17/08/2023 | Lectura y escritura de archivos planos "\Sesiones\Sesion3\Sesion3.py"  |
| 4       | 23/08/2023 | Agregar filas y columnas con pandas "\Sesiones\Sesion4\Sesion4.py"     |
| 5       | 24/08/2023 | Importar DataFrame de páginas "\Sesiones\Sesion5\Sesion5.py"           |